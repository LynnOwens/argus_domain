package net.tofweb.argus.domain;

public class TokenResponse {
	private String agentToken;

	public String getAgentToken() {
		return agentToken;
	}

	public void setAgentToken(String agentToken) {
		this.agentToken = agentToken;
	}
	
}
