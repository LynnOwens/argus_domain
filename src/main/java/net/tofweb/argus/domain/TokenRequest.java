package net.tofweb.argus.domain;

import java.io.File;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import net.tofweb.argus.domain.model.AgentModel;

@Entity
public class TokenRequest {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	private AgentModel agentModel;
	private File[] files;
	
	public TokenRequest() {
		super();
	}
	
	public TokenRequest(AgentModel agentModel, File[] files) {
		super();
		this.agentModel = agentModel;
		this.files = files;
	}

	public AgentModel getAgentModel() {
		return agentModel;
	}
	
	public void setAgentModel(AgentModel agentModel) {
		this.agentModel = agentModel;
	}

	public File[] getFiles() {
		return files;
	}

	public void setFiles(File[] files) {
		this.files = files;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
